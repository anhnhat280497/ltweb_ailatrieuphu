package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.Connection;

public final class register_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(2);
    _jspx_dependants.add("/ketnoi.jsp");
    _jspx_dependants.add("/index.html");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
 
    Class.forName("com.mysql.jdbc.Driver");
    Connection conn = DriverManager.getConnection(
            "jdbc:mysql://localhost:3306/ailatrieuphu", "root", "");
    Statement stm = conn.createStatement();

      out.write('\n');
      out.write('\n');
      out.write('\n');

    boolean error = true;
    String checkNull = "";
    String username = request.getParameter("username");
    if (username != "") {
        String sql = "insert into users value('" + username + "', "+0+")";
        try {
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            error = false;
        }
    } else error = false;
    if(!error) checkNull += "Username chưa được nhập hoặc đã được đăng ký!";

      out.write("\n");
      out.write("<script>\n");
      out.write("    const mess = '");
      out.print(checkNull);
      out.write("';\n");
      out.write("    const user = '");
      out.print(username);
      out.write("';\n");
      out.write("    if(mess !== \"\") alert(mess);\n");
      out.write("    else alert('Hello ' + user)\n");
      out.write("</script>\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!--\n");
      out.write("To change this license header, choose License Headers in Project Properties.\n");
      out.write("To change this template file, choose Tools | Templates\n");
      out.write("and open the template in the editor.\n");
      out.write("-->\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title>AI LA TRIEU PHU</title>\n");
      out.write("        <meta charset=\"UTF-8\">\n");
      out.write("        <link href=\"indexcss.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <form>\n");
      out.write("            <div class=\"main\">\n");
      out.write("                <div class=\"info\">\n");
      out.write("                    <button disabled=\"disabled\">User Name</button>\n");
      out.write("                    <input name=\"username\"/>\n");
      out.write("                    </br></br>\n");
      out.write("                    <input type=\"submit\" id=\"register\" name=\"register\" value=\"Register\" formmethod=\"post\" formaction=\"register.jsp\">\n");
      out.write("                    <input type=\"submit\" id=\"login\" name=\"login\" value=\"Login\" formmethod=\"post\" formaction=\"login.jsp\">\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </form>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
