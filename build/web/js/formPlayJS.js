/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

let clock, time, minute, second;
let numberQuestion = 0; // đếm số câu hỏi
let idCauHoi = null; // lấy id để lấy đáp án
let box = "boxA"; //box câu trả lời đúng, lấy từ database
let dapan = "A"; // đáp án đúng của câu hỏi đang hiển thị, lấy từ database
let listCH = new Array(16); //list 15 câu hỏi được ramdom theo id 
const audioBD = new Audio('../voice/batdauchoi.mp3'); //nhạc lúc vừa vào trang playgame

let audioHelp = new Audio('../voice/5050.mp3'); //âm thanh của 3 sự trợ giúp
const audioCH = new Audio('../voice/cauhoi.mp3');   // nhạc khi câu hỏi hiện lên, chờ người chơi trả lời
let checkAudio = 0; //0 là đang bật âm thanh(3 sự trợ giúp, đúng sai) , 1 là tắt
let checkListen = 0; //0 là đang bật nhạc nền, 1 là tắt
let clickNum = 1;   // kiểm tra nhạc
let usernameSS;     // username người dùng được lưu ở biến session
let ssDiem = 0; //diem cua người chơi 
let userhighScore = 0;
let highestScore = 0;

let dungcuocchoi = 0;   //nếu dừng cuộc chơi thì số tiền sẽ được tính ở câu hỏi đã trả lời được
//còn không sẽ tính ở mốc


const xmlhttp = new XMLHttpRequest();
const setssAudio = () => {
    xmlhttp.open("get", "../jsp/setcheckAudio.jsp?check=" + checkAudio + "^" + checkListen, false);
    xmlhttp.send();
};

const getssAudio = () => {
    xmlhttp.open("get", "../jsp/getcheckAudio.jsp", false);
    xmlhttp.send();
    let getsscheck = xmlhttp.responseText.split("^");
    checkAudio = getsscheck[0];
    checkListen = getsscheck[1];
    if(checkAudio != 0 && checkAudio != 1) checkAudio = 0;
    if(checkListen != 0 && checkListen != 1) checkListen = 0;
};
getssAudio(); // Lúc đầu chưa set nên khi khi get nó sẽ ra null. Nên audio lúc đầu bị tắt

if (checkAudio == 0) {
    document.getElementById('audioOn').src = '../image/audio.png';
} else {
    document.getElementById('audioOn').src = '../image/audioX.png';
}
if (checkListen == 0) {
    audioBD.play();
    document.getElementById('listenOn').src = '../image/listen.png';
} else {
    document.getElementById('listenOn').src = '../image/listenX.png';
}
const getValueSS = () => {  // lấy giá trị của biến session username
    xmlhttp.open("get", "../jsp/getvaluess.jsp", false);
    xmlhttp.send();
    const valuess = xmlhttp.responseText.trim();
    let array = new Array(2);
    array = valuess.split('^');
    usernameSS = array[0];
    ssDiem = array[1];
    userhighScore = ssDiem;
};
getValueSS();

function TaoSoNgauNhien(min, max) { //tạo 1 số ngẫu nhiên từ min đến max
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
const randomList = () => {  // random 15 câu, số từ 1 - 30, tương ứng với id trong database
    let i;
    for (i = 0; i < 15; i++) {
        const rann = TaoSoNgauNhien(1, 30);
        if (i > 0) {
            let j;
            for (j = 0; j < i; j++) {   //kiểm tra nếu trùng sẽ random lại
                if (rann == listCH[j]) {
                    i--;
                    break;
                }
            }
            if (j != i)
                continue;
        }
        listCH[i] = rann;
    }
};
randomList();

const trogiupKhanGia = () => {
    if (checkAudio == 0) {  // Người chơi mở audio mới được phát âm thanh
        audioHelp = new Audio('../voice/5050.mp3');
        audioHelp.pause();
        audioHelp.play();
    }
    document.getElementById("survey").style.display = 'block';
    document.getElementById("hideTable").style.display = 'block';
    document.getElementById("divlabelhide").style.display = 'block';
    document.getElementById("three").src = "../image/threexxx.png";
    document.getElementById("khangia").removeEventListener('click', trogiupKhanGia);
    let phantram = 0;   //Tạo các số ngẫu nhiên của phần trăm đáp án
    const dapan1 = TaoSoNgauNhien(55, 90);
    phantram = 100 - dapan1;
    const cau11 = TaoSoNgauNhien(0, phantram);
    phantram = 100 - dapan1 - cau11;
    const cau22 = TaoSoNgauNhien(0, phantram);
    phantram = 100 - dapan1 - cau11 - cau22;
    const cau33 = phantram;
    getDapAn();
    if (dapan == 'A') {
        document.getElementById("percentA").style.height = dapan1 + '%';
        document.getElementById("a").innerHTML = 'A<br>' + dapan1 + '%';
        document.getElementById("percentB").style.height = cau11 + '%';
        document.getElementById("b").innerHTML = 'B<br>' + cau11 + '%';
        document.getElementById("percentC").style.height = cau22 + '%';
        document.getElementById("c").innerHTML = 'C<br>' + cau22 + '%';
        document.getElementById("percentD").style.height = cau33 + '%';
        document.getElementById("d").innerHTML = 'D<br>' + cau33 + '%';
    } else if (dapan == 'B') {
        document.getElementById("percentB").style.height = dapan1 + '%';
        document.getElementById("b").innerHTML = 'B<br>' + dapan1 + '%';
        document.getElementById("percentA").style.height = cau11 + '%';
        document.getElementById("a").innerHTML = 'A<br>' + cau11 + '%';
        document.getElementById("percentC").style.height = cau22 + '%';
        document.getElementById("c").innerHTML = 'C<br>' + cau22 + '%';
        document.getElementById("percentD").style.height = cau33 + '%';
        document.getElementById("d").innerHTML = 'D<br>' + cau33 + '%';
    } else if (dapan == 'C') {
        document.getElementById("percentC").style.height = dapan1 + '%';
        document.getElementById("c").innerHTML = 'C<br>' + dapan1 + '%';
        document.getElementById("percentB").style.height = cau11 + '%';
        document.getElementById("b").innerHTML = 'B<br>' + cau11 + '%';
        document.getElementById("percentA").style.height = cau22 + '%';
        document.getElementById("a").innerHTML = 'A<br>' + cau22 + '%';
        document.getElementById("percentD").style.height = cau33 + '%';
        document.getElementById("d").innerHTML = 'D<br>' + cau33 + '%';
    } else if (dapan == 'D') {
        document.getElementById("percentD").style.height = dapan1 + '%';
        document.getElementById("d").innerHTML = 'D<br>' + dapan1 + '%';
        document.getElementById("percentB").style.height = cau11 + '%';
        document.getElementById("b").innerHTML = 'B<br>' + cau11 + '%';
        document.getElementById("percentC").style.height = cau22 + '%';
        document.getElementById("c").innerHTML = 'C<br>' + cau22 + '%';
        document.getElementById("percentA").style.height = cau33 + '%';
        document.getElementById("a").innerHTML = 'A<br>' + cau33 + '%';
    }
};
document.getElementById("khangia").addEventListener('click', trogiupKhanGia);
// Trợ giúp khán giả

const closeTable = () => {
    document.getElementById("survey").style.display = 'none';
    document.getElementById("hideTable").style.display = 'none';
    document.getElementById("divlabelhide").style.display = 'none';
};
document.getElementById("hideTable").addEventListener('click', closeTable);
// OK của bảng phần trăm khán giả

const trogiupGoiDT = () => {
    if (checkAudio == 0) {
        audioHelp = new Audio('../voice/5050.mp3');
        audioHelp.pause();
        audioHelp.play();
    }
    document.getElementById("comment").style.display = 'block';
    document.getElementById("hideComment").style.display = 'block';
    document.getElementById("two").src = "../image/twoxxx.png";
    document.getElementById("callphone").removeEventListener('click', trogiupGoiDT); // off sự kiện click

    getDapAn();
    document.getElementById("hihi").value = dapan;
};
document.getElementById("callphone").addEventListener('click', trogiupGoiDT);
// Trợ giúp gọi điện cho người thân

const closeComment = () => {
    document.getElementById("comment").style.display = 'none';
    document.getElementById("hideComment").style.display = 'none';
};
document.getElementById("hideComment").addEventListener('click', closeComment);
//OK của trợ giúp gọi điện cho người thân

const trogiup5050 = () => {
    if (checkAudio == 0) {
        audioHelp = new Audio('../voice/5050.mp3');
        audioHelp.pause();
        audioHelp.play();
    }
    document.getElementById("one").src = "../image/onexxx.png";
    document.getElementById("50-50").removeEventListener('click', trogiup5050);
    const ran = Math.floor((Math.random() * 3) + 1);
    getDapAn();
    let intDA = 0;
    if (dapan == 'A') {
        if (ran == 1) {
            document.getElementById('boxC').value = "";
            document.getElementById('boxD').value = "";
            document.getElementById('boxC').removeEventListener('click', clickDapAnC);
            document.getElementById('boxD').removeEventListener('click', clickDapAnD);
        } else if (ran == 2) {
            document.getElementById('boxB').value = "";
            document.getElementById('boxD').value = "";
            document.getElementById('boxB').removeEventListener('click', clickDapAnB);
            document.getElementById('boxD').removeEventListener('click', clickDapAnD);
        } else if (ran == 3) {
            document.getElementById('boxB').value = "";
            document.getElementById('boxC').value = "";
            document.getElementById('boxC').removeEventListener('click', clickDapAnC);
            document.getElementById('boxB').removeEventListener('click', clickDapAnB);
        }
    }
    if (dapan == 'B') {
        if (ran == 1) {
            document.getElementById('boxC').value = "";
            document.getElementById('boxD').value = "";
            document.getElementById('boxC').removeEventListener('click', clickDapAnC);
            document.getElementById('boxD').removeEventListener('click', clickDapAnD);
        } else if (ran == 2) {
            document.getElementById('boxA').value = "";
            document.getElementById('boxD').value = "";
            document.getElementById('boxA').removeEventListener('click', clickDapAnA);
            document.getElementById('boxD').removeEventListener('click', clickDapAnD);
        } else if (ran == 3) {
            document.getElementById('boxA').value = "";
            document.getElementById('boxC').value = "";
            document.getElementById('boxC').removeEventListener('click', clickDapAnC);
            document.getElementById('boxA').removeEventListener('click', clickDapAnA);
        }
    }
    if (dapan == 'C') {
        if (ran == 1) {
            document.getElementById('boxB').value = "";
            document.getElementById('boxD').value = "";
            document.getElementById('boxB').removeEventListener('click', clickDapAnB);
            document.getElementById('boxD').removeEventListener('click', clickDapAnD);
        } else if (ran == 2) {
            document.getElementById('boxA').value = "";
            document.getElementById('boxD').value = "";
            document.getElementById('boxA').removeEventListener('click', clickDapAnA);
            document.getElementById('boxD').removeEventListener('click', clickDapAnD);
        } else if (ran == 3) {
            document.getElementById('boxA').value = "";
            document.getElementById('boxB').value = "";
            document.getElementById('boxA').removeEventListener('click', clickDapAnA);
            document.getElementById('boxB').removeEventListener('click', clickDapAnB);
        }
    }
    if (dapan == 'D') {
        if (ran == 1) {
            document.getElementById('boxB').value = "";
            document.getElementById('boxC').value = "";
            document.getElementById('boxC').removeEventListener('click', clickDapAnC);
            document.getElementById('boxB').removeEventListener('click', clickDapAnB);
        } else if (ran == 2) {
            document.getElementById('boxA').value = "";
            document.getElementById('boxC').value = "";
            document.getElementById('boxC').removeEventListener('click', clickDapAnC);
            document.getElementById('boxA').removeEventListener('click', clickDapAnA);
        } else if (ran == 3) {
            document.getElementById('boxA').value = "";
            document.getElementById('boxB').value = "";
            document.getElementById('boxA').removeEventListener('click', clickDapAnA);
            document.getElementById('boxB').removeEventListener('click', clickDapAnB);
        }
    }
};
document.getElementById("50-50").addEventListener('click', trogiup5050);
// Trợ giúp 50-50
const dungChoi = () => {
    clearInterval(clock);
    audioCH.pause();

    if (numberQuestion > userhighScore) {
        userhighScore = numberQuestion - 1;
        saveScore();
    }

    if (numberQuestion > 15) {
        const audio = new Audio('../voice/batdauchoi.mp3');
        if (checkAudio == 0)
            audio.play();
    } else {
        const audio = new Audio('../voice/sai.mp3');
        if (checkAudio == 0)
            audio.play();
    }

    if (numberQuestion == 1)
        document.getElementById('result').innerHTML = 'THUA CUỘC';
    else if (numberQuestion < 6) {
        if (dungcuocchoi != 1)
            document.getElementById('result').innerHTML = 'THUA CUỘC';
        else {
            const t = document.getElementById(`t${numberQuestion - 1}`).textContent;
            document.getElementById('result').innerHTML = 'THẮNG CUỘC</br>' + t + '.000';
        }
    } else if (numberQuestion > 15) {
        document.getElementById('result').innerHTML = 'THẮNG CUỘC</br>150.000.000';
    } else if (numberQuestion >= 6) {
        if (dungcuocchoi == 1) {
            const t = document.getElementById(`t${numberQuestion - 1}`).textContent;
            document.getElementById('result').innerHTML = 'THẮNG CUỘC</br>' + t + '.000';
        } else {
            if (numberQuestion >= 11) {
                document.getElementById('result').innerHTML = 'THẮNG CUỘC</br>22.000.000';
            } else {
                document.getElementById('result').innerHTML = 'THẮNG CUỘC</br>2.000.000';
            }
        }
    }
    document.getElementById("timeOut").style.display = 'none';
    document.getElementById("comment").style.display = 'none';
    document.getElementById("hideComment").style.display = 'none';
    document.getElementById("result").style.display = 'block';
    document.getElementById("replay").style.display = 'block';
    document.getElementById("contentques").style.display = 'none';
    document.getElementById("khangia").removeEventListener('click', trogiupKhanGia);
    document.getElementById("callphone").removeEventListener('click', trogiupGoiDT);
    document.getElementById("50-50").removeEventListener('click', trogiup5050);
};
const clickstop = () => {
    var confirmStop = confirm("Nhấn OK để dừng chơi!");
    if (confirmStop) {
        dungcuocchoi = 1;
        closeTable();
        dungChoi();
    }
};
document.getElementById("pause").addEventListener('click', clickstop);
// Click dừng chơi

const replay = () => {
    setssAudio();
    clickNum = 1;
    document.location.href = "../html/formPlay.html";
//    document.getElementById("start").style.display = 'block';
//    document.getElementById("khangia").addEventListener('click', trogiupKhanGia);
//    document.getElementById("callphone").addEventListener('click', trogiupGoiDT);
//    document.getElementById("50-50").addEventListener('click', trogiup5050);
//    document.getElementById("three").src = "../image/three.png";
//    document.getElementById("two").src = "../image/two.png";
//    document.getElementById("one").src = "../image/one.png";
//    document.getElementById("result").style.display = 'none';
//    document.getElementById("replay").style.display = 'none';
    //xmlhttp.open("post", "../html/formPlay.html", false);
    //xmlhttp.open("post", "/GameAiLaTrieuPhu/ControllerServlet?yc=playgame", false);
    //xmlhttp.send();
    //document.write(xmlhttp.responseText);
    showHighestScore();
};
document.getElementById("replay").addEventListener('click', replay);
// Click chơi lại

const clickDapAnA = () => {
    document.getElementById('boxA').style.backgroundColor = 'green';
    document.getElementById('boxC').style.backgroundColor = 'activecaption';
    document.getElementById('boxB').style.backgroundColor = 'activecaption';
    document.getElementById('boxD').style.backgroundColor = 'activecaption';
};
document.getElementById('boxA').addEventListener('click', clickDapAnA);
const clickDapAnB = () => {
    document.getElementById('boxB').style.backgroundColor = 'green';
    document.getElementById('boxC').style.backgroundColor = 'activecaption';
    document.getElementById('boxA').style.backgroundColor = 'activecaption';
    document.getElementById('boxD').style.backgroundColor = 'activecaption';
};
document.getElementById('boxB').addEventListener('click', clickDapAnB);
const clickDapAnC = () => {
    document.getElementById('boxC').style.backgroundColor = 'green';
    document.getElementById('boxB').style.backgroundColor = 'activecaption';
    document.getElementById('boxA').style.backgroundColor = 'activecaption';
    document.getElementById('boxD').style.backgroundColor = 'activecaption';
};
document.getElementById('boxC').addEventListener('click', clickDapAnC);
const clickDapAnD = () => {
    document.getElementById('boxD').style.backgroundColor = 'green';
    document.getElementById('boxB').style.backgroundColor = 'activecaption';
    document.getElementById('boxC').style.backgroundColor = 'activecaption';
    document.getElementById('boxA').style.backgroundColor = 'activecaption';
};
document.getElementById('boxD').addEventListener('click', clickDapAnD);
const getDapAn = () => {
    xmlhttp.open("get", "../jsp/selectanswer.jsp?id=" + idCauHoi, false);
    xmlhttp.send();
    dapan = xmlhttp.responseText.trim();
};
const confirmDA = () => {
    var confirmStop = confirm("Bạn đã chắc chắn?");
    if (confirmStop) {
        document.getElementById("comment").style.display = 'none';
        document.getElementById("hideComment").style.display = 'none';
        closeTable();
        getDapAn();
        box = `box${dapan}`;
        const boxColor = document.getElementById(box).style.backgroundColor;
        if (boxColor == 'green') {
            const audio = new Audio('../voice/dung.mp3');
            if (checkAudio == 0)
                audio.play();
            document.getElementById(`cau${numberQuestion}`).style.backgroundColor = '#33ff33';
            if (numberQuestion > 1) {
                document.getElementById(`cau${numberQuestion - 1}`).style.backgroundColor = '#7fb6bc';
                if ((numberQuestion - 1) % 5 == 0)
                    document.getElementById(`cau${numberQuestion - 1}`).style.backgroundColor = 'coral';
                if (numberQuestion % 5 == 0)
                    document.getElementById(`cau${numberQuestion}`).style.backgroundColor = 'coral';
            }
            clickStart();
        } else
        {
            const audio = new Audio('../voice/sai.mp3');
            if (checkAudio == 0)
                audio.play();
            document.getElementById(box).style.background = 'yellow';
            alert(dapan);
            dungChoi();
            audioCH.pause();
        }
    }
};
document.getElementById('submit').addEventListener('click', confirmDA);
const clickStart = () => {
    time = 60;
    minute = parseInt(time / 60);
    second = time % 60;
    clearInterval(clock);
    clock = setInterval('startTime()', 1000);
    clickNum = 2;
    audioBD.pause();
    document.getElementById("highscore").style.display = 'none';
    document.getElementById("timeOut").style.display = 'block';
    document.getElementById('boxA').addEventListener('click', clickDapAnA);
    document.getElementById('boxB').addEventListener('click', clickDapAnB);
    document.getElementById('boxC').addEventListener('click', clickDapAnC);
    document.getElementById('boxD').addEventListener('click', clickDapAnD);
    document.getElementById(box).style.backgroundColor = 'activecaption';
    document.getElementById('helpmoney').style.display = 'block';
    document.getElementById('contentques').style.display = 'block';
    document.getElementById('start').style.display = 'none';
    numberQuestion += 1;
    if (numberQuestion > 15)
        dungChoi();
    else {
        const idcauhoi = listCH[numberQuestion - 1];
        xmlhttp.open("get", "../jsp/selectquestion.jsp?idcauhoi=" + idcauhoi, false);
        xmlhttp.send();
        const cauHoi = xmlhttp.responseText.trim();
        if (cauHoi == "0") {
            alert('Mời bạn đăng nhập!');
            document.location.href = "../html/index.html";
            //xmlhttp.open("post", "../html/index.html", false);
            //xmlhttp.open("post", "/GameAiLaTrieuPhu/ControllerServlet?yc=index", false);
            //xmlhttp.send();
            //document.write(xmlhttp.responseText);
        } else {
            if (checkListen == 0)
                audioCH.play();
            const arrayCH = cauHoi.split("^");
            document.getElementById('question').value = "Câu " + numberQuestion + ": " + arrayCH[0];
            document.getElementById('boxA').value = "A. " + arrayCH[1];
            document.getElementById('boxB').value = "B. " + arrayCH[2];
            document.getElementById('boxC').value = "C. " + arrayCH[3];
            document.getElementById('boxD').value = "D. " + arrayCH[4];
            idCauHoi = arrayCH[5];
        }
    }
};
document.getElementById('start').addEventListener('click', clickStart);
const pauseAudio = () => {
    if (checkAudio == 0) {
        document.getElementById('audioOn').src = '../image/audioX.png';
        audioHelp.pause();
        checkAudio = 1;
    } else {
        document.getElementById('audioOn').src = '../image/audio.png';
        checkAudio = 0;
    }
};
document.getElementById('audioOn').addEventListener('click', pauseAudio);
const pauseListen = () => {
    if (checkListen == 0) {
        document.getElementById('listenOn').src = '../image/listenX.png';
        audioCH.pause();
        audioBD.pause();
        checkListen = 1;
    } else {
        if (clickNum == 1)
            audioBD.play();
        if (clickNum == 2)
            audioCH.play();
        checkListen = 0;
        document.getElementById('listenOn').src = '../image/listen.png';
    }
};
document.getElementById('listenOn').addEventListener('click', pauseListen);
const logout = () => {
    xmlhttp.open("get", "../jsp/logout.jsp", false);
    xmlhttp.send();
    document.location.href = "../html/index.html";
    //xmlhttp.open("post", "../html/index.html", false);
    //xmlhttp.open("get", "/GameAiLaTrieuPhu/ControllerServlet?yc=index", false);
    //xmlhttp.send();
    //document.write(xmlhttp.responseText);
};
document.getElementById('logout').addEventListener('click', logout);

const startTime = () => {
    if (second === 0) {
        if (minute === 0) {
            clearInterval(clock);
            //
            const audio = new Audio('../voice/sai.mp3');
            if (checkAudio == 0)
                audio.play();
            document.getElementById(box).style.background = 'yellow';
            alert(dapan);
            dungChoi();
            audioCH.pause();
        } else {
            minute -= 1;
            second = 59;
        }
    } else {
        second -= 1;
    }
    document.getElementById('timeOut').innerHTML = `${checkTime(minute)}:${checkTime(second)}`;
};
const checkTime = (i) => {
    if (i < 10) {
        i = `0${i}`;
    }
    return i;
};
const saveScore = () => {
    xmlhttp.open("get", "../jsp/updateScore.jsp?diem=" + userhighScore, false);
    xmlhttp.send();
};

const gethighScore = () => {
    xmlhttp.open("get", "../jsp/gethighscore.jsp?", false);
    xmlhttp.send();
    highestScore = xmlhttp.responseText.trim();
};
const showHighestScore = () => {
    gethighScore();
    document.getElementById("hscore").innerHTML = highestScore;
};
showHighestScore();