/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ailatrieuphu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 *
 * @author Nguyen Anh Nhat
 */
public class CSDL {
    static Connection con;
    static Statement stm;
    public static void moKetNoi(String database, String username, String password){
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + database, username, password);
        
            stm = con.createStatement();
            System.out.println("Mo ket noi CSDL thanh cong!");
        } catch (Exception ex){
            System.out.println("Loi: " + ex.getMessage());
        }
    }
    public static void dongKetNoi() {
        try{
            stm.close();
            con.close();
            System.out.println("Dong ket noi thanh cong!");
        } catch(Exception ex) {
            System.out.println("Loi: " + ex.getMessage());
        }
    }
    public static void main(String[] args) {
//        CSDL.moKetNoi("ailatrieuphu", "root", "");
//        CSDL.dongKetNoi();
    }
}
