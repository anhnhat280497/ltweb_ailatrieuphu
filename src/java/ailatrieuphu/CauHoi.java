/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ailatrieuphu;

import java.sql.ResultSet;
import java.util.Random;

/**
 *
 * @author Nguyen Anh Nhat cutws
 */
public class CauHoi {
    int id;
    String noidung;
    String cauA;
    String cauB;
    String cauC;
    String cauD;
    char dapAn;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNoidung() {
        return noidung;
    }

    public void setNoidung(String noidung) {
        this.noidung = noidung;
    }

    public String getCauA() {
        return cauA;
    }

    public void setCauA(String cauA) {
        this.cauA = cauA;
    }

    public String getCauB() {
        return cauB;
    }

    public void setCauB(String cauB) {
        this.cauB = cauB;
    }

    public String getCauC() {
        return cauC;
    }

    public void setCauC(String cauC) {
        this.cauC = cauC;
    }

    public String getCauD() {
        return cauD;
    }

    public void setCauD(String cauD) {
        this.cauD = cauD;
    }

    public char getDapAn() {
        return dapAn;
    }

    public void setDapAn(char dapAn) {
        this.dapAn = dapAn;
    }

    public CauHoi(int id, String noidung, String cauA, String cauB, String cauC, String cauD, char dapAn) {
        this.id = id;
        this.noidung = noidung;
        this.cauA = cauA;
        this.cauB = cauB;
        this.cauC = cauC;
        this.cauD = cauD;
        this.dapAn = dapAn;
    }

    public static CauHoi layCauHoi(String idcauhoi) {
        CSDL.moKetNoi("ailatrieuphu", "root", "");
        CauHoi ch = null;
        try {
            String sql = "select * from cauhoi where ID = " + idcauhoi;
            ResultSet rs = CSDL.stm.executeQuery(sql);
            if(rs.next()) {
                ch = new CauHoi( rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                rs.getString(5), rs.getString(6), ' ');
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex);
        }
        CSDL.dongKetNoi();
        return ch;
    }
    public static String layDapAn(String idCH) {
        CSDL.moKetNoi("ailatrieuphu", "root", "");
        String dapan = "";
        try {
            String sql = "select DAPAN from cauhoi where ID = " + idCH;
            ResultSet rs = CSDL.stm.executeQuery(sql);
            if(rs.next()) {
                dapan = rs.getString("DAPAN");
            }
        } catch (Exception ex) {
            System.out.println("Loi: " + ex);
        }
        CSDL.dongKetNoi();
        return dapan;
    }
    
    public static void main(String[] args) {
        CauHoi ch = CauHoi.layCauHoi("2");
        if(ch != null) System.out.println(ch);
        String a = ch.layDapAn("1");
        System.out.println(ch.getId() + ch.getNoidung() + ch.getCauA());
        System.out.println(a);
    }
  
}
