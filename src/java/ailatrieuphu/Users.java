/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ailatrieuphu;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Nguyen Anh Nhat
 */
public class Users {
    String username;
    int diem;

    public Users(String username, int diem) {
        this.username = username;
        this.diem = diem;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getDiem() {
        return diem;
    }

    public void setDiem(int diem) {
        this.diem = diem;
    }
    
    public boolean dangKy() {
        CSDL.moKetNoi("ailatrieuphu", "root", "");
        boolean ketqua;
        try {
            String sql = "insert into users values('" + this.username + "', " + this.diem + ")";
            CSDL.stm.executeUpdate(sql);
            ketqua = true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ketqua = false;
        }
        CSDL.dongKetNoi();
        return ketqua;
    }

    public Users dangNhap() {
        CSDL.moKetNoi("ailatrieuphu", "root", "");
        Users user = null;
        try {
            String sql = "select * from users where USERNAME='" + this.username + "'";
            ResultSet rs = CSDL.stm.executeQuery(sql);
            if (rs.next()) {
                user = new Users(rs.getString("USERNAME"), rs.getInt("DIEM"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            user = null;
        }
        CSDL.dongKetNoi();
        return user;
    }
    
    public boolean nhapDiem(String diem, String username) {
        CSDL.moKetNoi("ailatrieuphu", "root", "");
        boolean ketqua;
        try {
            String sql = "update users set DIEM=" + diem + " where USERNAME='" + username + "'";
            CSDL.stm.executeUpdate(sql);
            ketqua = true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ketqua = false;
        }
        CSDL.dongKetNoi();
        return ketqua;
    }
    
    public String highScore() {
        CSDL.moKetNoi("ailatrieuphu", "root", "");
        String high = "";
        try{
            String sql = "select max(DIEM) from users";
            ResultSet rs = CSDL.stm.executeQuery(sql);
            rs.next();
            high = rs.getString(1);
        } catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        CSDL.dongKetNoi();
        return high;
    }
    
    public static void main(String[] args) {
        
         Users user = new Users("c", 0);
         System.out.println(user);
         boolean ketqua = user.dangKy();
         if (ketqua == true) {
         System.out.println("dang ky thanh cong");
         }else{
         System.out.println("dang ky khong thanh cong");
         }
         
        
        Users sinhvien = new Users("c", 8);
        sinhvien = sinhvien.dangNhap();
        if (sinhvien == null) {
            System.out.println("dang nhap sai!");
        } else {
            System.out.println("dang nhap thanh cong!");
            System.out.println(sinhvien);
        }
        System.out.println(user.highScore());
        System.out.println(user.nhapDiem("3","c"));
    }
}
